#!/bin/bash

PS3='Please enter your choice: '
options=("create" "run" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "create")
	    cd imagetemp
	    docker build -t nginx:temp .
	    cd ../
	    docker-compose down
	    docker-compose up -d
	    CHECK=0
	    while [ $CHECK -ne 2 ]
	      do
	        CHECK=`docker-compose ps|grep "Exit "|wc -l`
		echo $CHECK
		sleep 10
	      done	
	        #ERRCHECK=`docker-compose ps|grep "Exit "|awk {'print $5'}`
		#if [ $ERRCHECK -ne "0" ]
		#then 
		#      echo "exit $ERRCHECK"
		#fi
            mv currency_back_end/target/*.jar imageback/
            cd imageback
            docker build -t currency_back_end:latest .
   	    rm -rf *.jar
	    cd ../
	    mv currency_front_end/dist/currency-front/ imagefront/
            cd imagefront
            docker build -t currency_front_end:latest .
	    rm -rf currency-front/
	    break
	    ;;
        "run")
	    docker-compose -f run_docker.yml up -d
	    break
	    ;;
        "Quit")
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done
